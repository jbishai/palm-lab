'''
This program takes as input the assembly for a bacterial genome, prokka annotations of genes, and
creates mutant genomes according to user specified parameters
'''

import argparse
import os
import random
import errno
#import Bio
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

def get_gene_lengths(genome):
    '''gets legnth of all contigs in a genome'''
    lengths = []
    for rec in genome:
        lengths.append(len(rec.seq))
    return lengths

def read_in_genome(genome_path):
    '''read in genome file'''
    genome = SeqIO.parse(genome_path, 'fasta')
    lengths = get_gene_lengths(genome)
    genome = SeqIO.parse(genome_path, 'fasta')
    return(genome, lengths)

def setup_arguments():
    ''' setup arguments'''
    parser = argparse.ArgumentParser(
        description='create genomes containing mutations corresponding to EMS mutagenesis')
    parser.add_argument('--genome-path', type=str, help='filepath of fasta with genome of organism')
    parser.add_argument('--n-mutations', type=int, help='number of mutations to produce')
    parser.add_argument('--outdir', type=str, help='where to output mutant genomes')
    parser.add_argument('--basename', type=str, help='base filename for each mutant')
    parser.add_argument('--n-iterations', type=str, help='number of mutants to generate')
    return parser.parse_args()

def generate_mutation_array(length, n_mutations):
    '''generate n mutational positions from 0 to length of genome '''
    mutation_array = [random.randint(0, length) for x in range(0, n_mutations)]
    return sorted(mutation_array)

def write_mutant(offset_iter, genome, cur_gene, mutation_dict, outfile):
    '''
    All mutations in this given stretch of sequence have been accounted for
    create a new sequence object with those mutations and a description of those
    mutations
    '''
    seq = list(cur_gene.seq)
    info = cur_gene.id
    for mutation in mutation_dict:
        seq[mutation] = mutation_dict[mutation]
        info = '%s:%i%s' % (info, mutation, mutation_dict[mutation])

    mutated_seq = SeqRecord(Seq(''.join(seq)), id=info, name=info, description=info)
    next_off = next(offset_iter)
    next_gene = next(genome, None)
    cleared_dict = {}
    SeqIO.write(mutated_seq, outfile, 'fasta')
    return next_off, next_gene, cleared_dict

def mutate_contigs(genome, gene_lengths, mutation_seed_positions, args, iteration):
    '''
    for each mutation in the seed position list, find position in contig
    then find the next T/A and mutate to a C/G
    '''
    running_total_gene_lengths = [gene_lengths[0]]
    for x in range(1, len(gene_lengths)):
        running_total_gene_lengths.append(running_total_gene_lengths[x - 1] + gene_lengths[x])

    gene_offset_iter = iter(running_total_gene_lengths)
    cur_offset = 0
    cur_gene = next(genome)
    mutation_dict = {}
    #next(gene_offset_iter)
    basefile = '%s/%s' % (args.outdir, args.basename)
    with open('%s.%i.fna' % (basefile, iteration), 'w') as outfile:
        for seed_pos in mutation_seed_positions:
            mut_found = False
            pos = seed_pos - cur_offset
            while len(cur_gene.seq) + cur_offset <= seed_pos:
                cur_offset, cur_gene, mutation_dict = \
                write_mutant(gene_offset_iter, genome, cur_gene, mutation_dict, outfile)
                #cur_gene = next(genome)
                #cur_offset = next(gene_offset_iter)
                pos = seed_pos - cur_offset

            while pos < len(cur_gene.seq) and not mut_found:
                cur_base = cur_gene.seq[pos]
                if cur_base in ('T', 'A'):
                    mut_found = True
                    # Check what the actual transitions are and replace if needed
                    if cur_base == 'T':
                        mutation_dict[pos] = 'C'
                    if cur_base == 'A':
                        mutation_dict[pos] = 'G'
                    break
                pos += 1
                if pos >= len(cur_gene.seq):
                    cur_offset, cur_gene, mutation_dict = \
                    write_mutant(gene_offset_iter, genome, cur_gene, mutation_dict, outfile)
                    #cur_gene = next(genome)
                    #cur_offset = next(gene_offset_iter)
                    pos = 0
        #write last mutation
        if cur_gene is not None:
            cur_offset, cur_gene, mutation_dict = \
            write_mutant(gene_offset_iter, genome, cur_gene, mutation_dict, outfile)
        #write rest of mutations
        SeqIO.write(genome, outfile, 'fasta')

def generate_file_path(args):
    ''' if outdir doesn't exist, make outdir'''
    filepath = args.outdir
    try:
        os.makedirs(filepath)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def main():
    '''main method'''
    args = setup_arguments()
    if args.outdir:
        generate_file_path(args)
    iterations = args.iterations
    genome, gene_lengths = read_in_genome(args.genome_path)

    mutation_seed_positions = generate_mutation_array(sum(gene_lengths), args.n_mutations)
    #mutation_seed_positions = generate_mutation_array(537864, args.n_mutations)
    #mutation_seed_positions = [100, 200, 300, 96100, 96200, 1000000]
    for iteration in range(iterations):
        mutate_contigs(genome, gene_lengths, mutation_seed_positions, args, iteration)
        
    #print(mutation_seed_positions)

main()
