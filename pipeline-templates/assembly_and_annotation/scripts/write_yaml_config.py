import glob
import re
import os

def main():
    sampDict = {}
    for x in glob.glob('../data/reads/*'):
        sample = re.search('(?<=reads/).*', x).group(0)
        r1Path = glob.glob(x + '/*')[0]
        r2Path = glob.glob(x + '/*')[1]
        print(r1Path)
        print(r2Path)

        r1New = '../data/renamed_reads/' + sample + '_R1_001.fastq.gz'
        r2New = '../data/renamed_reads/' + sample + '_R2_001.fastq.gz' 
        deSuffixed = re.search('.*(?=_R1_)', r1Path).group(0)
        os.system('ln ' + r1Path + ' ' + r1New)
        os.system('ln ' + r2Path + ' ' + r2New)
        
        sampDict[sample] = deSuffixed

                
    with open('config.yaml', 'w') as yaml:
        yaml.write('samples:\n')
        for samp in sampDict:
            yaml.write('  %s: %s\n' % (samp, samp))
            
                   

main()
